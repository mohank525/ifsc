from django.conf.urls import url
from django.core.urlresolvers import reverse
from optimus import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
  url(r'^$', views.home, name='home'),
  url(r'^index/', views.index, name='index'),
  url(r'^bank/$', views.bank, name='bank'),
  url(r'^district/$', views.district, name='district'),
  url(r'^area/$', views.area, name='area'),
  url(r'^output/$', views.output, name='output'),
  url(r'^oldmicr/$', views.oldmicr, name='oldmicr'),
  url(r'^micr/$', views.micr, name='micr'),
  url(r'^contact/$', views.contact, name='contact'),
  url(r'^privacy/$', views.privacy, name='privacy'),
  url(r'^test/$', views.test, name='test'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

