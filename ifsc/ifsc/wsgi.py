"""
WSGI config for ifsc project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
import static
wsgi_app = static.Cling('/var/www/ifsc/ifsc/ifsc/templates')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ifsc.settings")

application = get_wsgi_application()
